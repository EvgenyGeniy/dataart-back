const Router = require('express')
const router = Router();
const User = require('../models/user');

const users = [
    {
        id:'1',
        cardNumber:'123456789',
        firstName:'Evgeny',
        secondName:'Ponomarev',
        balance:'10000000',
        birthday:'23-12-1999',
        pin:'1234'
    },
    {
        id:'2',
        cardNumber:'111111111',
        firstName:'Ivan',
        secondName:'Ivanov',
        balance:'100',
        birthday:'11-11-1111',
        pin:'4567'
    },
    {
        id:'1',
        cardNumber:'222222222',
        firstName:'Petr',
        secondName:'Petrovich',
        balance:'1000',
        birthday:'22-22-2222',
        pin:'8901'
    }
]

router.get('/check',async (req,res)=>{
    const user = users.find(elem=>elem.cardNumber === req.query.card);
    if(!user){
        res.status(401).json('User not found');
    }
    if(user.pin !== req.query.pin){
        res.status(401).json('Invalid pin');
    }
    res.send({id:user.id})
})

router.get('/info',async (req,res)=>{
   // const user = await User.find() //mongo doesnt work well so i used an array
    const user = users.find(elem=>elem.id === req.query.id);
    res.send({user});
})

router.put('/cache', async (req,res)=>{
    const user = users.find(elem=>elem.id === req.query.id);
    if(Number(req.body.balance) < Number(user.balance)){
        user.balance -= req.body.balance;
        res.status(200).json('ok');
        return
    }
    res.status(400).json('Invalid sum');
})


module.exports = router;
