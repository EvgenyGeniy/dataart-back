const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const app = express();
const db = require('./config')
const cors = require('cors')
const router = require('./routers/userRouter');
app.use(cors())
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use('/',router)
mongoose.connect(db.mongoUrl,{useNewUrlParser:true,useUnifiedTopology:true}).then(()=>{
    console.log('connected');
})
    .catch((err)=>console.log(err));

mongoose.set('useFindAndModify', false);

app.listen(5000, function () {
    console.log('Example app listening on port 3000!');
});
