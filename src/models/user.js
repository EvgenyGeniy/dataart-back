const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    cardNumber:{
        type:String,
        required:true
    },
    firstName:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        required:true
    },
    secondName:{
        type:String,
        required:true
    },
    balance:{
        type:String,
        required:true
    }
});

const User = mongoose.model('Users',UserSchema);

module.exports = User;
